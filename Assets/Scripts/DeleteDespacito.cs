﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteDespacito : MonoBehaviour
{
    public float dispelTime;

    private SpriteRenderer sprite;
    private Color color;
    private bool active = false;
    private Color baseColor;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        color = sprite.color;
        baseColor = sprite.color;
    }

    void Update()
    {
        if (active)
        {
            color.a -= (Time.deltaTime * 0.33f) / dispelTime;
            sprite.color = color;
            if (sprite.color.a <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void Reset()
    {
        gameObject.SetActive(true);
        sprite.color = baseColor;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        active = true;
    }
}