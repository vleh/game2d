﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodCheckpoint : MonoBehaviour
{
    public string key;

    private GameManagersito gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManagersito>();
    }

    private void Update()
    {
        if (Input.GetKey(key))
        {
            gm.lastCheckPointPos = transform.position;
            gm.player.transform.position = gm.lastCheckPointPos;
            gm.player.SetActive(true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            gm.lastCheckPointPos = transform.position;
        }
    }
}

