﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagersito : MonoBehaviour
{
    public GameObject startPoint;
    private static GameManagersito instance;
    public GameObject player;
    public GameObject text;
    public GameState state;
    public float currentTime;
    public GameObject endGame;

    public GameObject checkPointPrefab;
    private bool practice;

    public List<GameObject> checkpointList;
    int index = 0;

    public Vector2 lastCheckPointPos;

    void Awake()
    {
        practice = state.practice;

        if (state.endedLastGame)
        {
            state.endedLastGame = false;
            state.deaths = 0;
        }

        if (state.endedLastPractice)
        {
            state.endedLastPractice = false;
            state.practiceDeaths = 0;
        }
        lastCheckPointPos = startPoint.transform.position;
        currentTime = 0f;
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        if(player.active)
        {
            if (Input.GetKeyDown("z") && practice)
            {
                checkpointList.Add(Instantiate(checkPointPrefab, player.transform.position, Quaternion.identity));
                lastCheckPointPos = checkpointList[index].transform.position;
                index++;
            }
        }

        if (Input.GetKeyDown("x") && practice && checkpointList.Count != 0)
        {
            index = checkpointList.Count - 1;
            Destroy(checkpointList[index]);
            checkpointList.RemoveAt(index);
            if (checkpointList.Count != 0)
            {
                lastCheckPointPos = checkpointList[index - 1].transform.position;
            }
            else
            {
                lastCheckPointPos = startPoint.transform.position;
            }
        }
        if (!player.active)
        {
            int deaths;
            if (!practice) {
                deaths = state.deaths + 1;
            } else
            {
                deaths = state.practiceDeaths + 1;
            }
            text.GetComponent<Text>().text = "Has muerto un total de " + deaths + " veces\nPresiona R para reaparecer";
            text.SetActive(true);
            if (Input.GetKeyDown("r"))
            {
                if (!practice)
                {
                    state.deaths++;
                    state.totalDeaths++;
                } else
                {
                    state.practiceDeaths++;
                }
                text.SetActive(false);
                gameObject.SetActive(text);
                player.transform.position = lastCheckPointPos;
                player.SetActive(true);
                if (!practice)
                {
                    currentTime = 0;
                }
            }
        }
        if (endGame != null && endGame.GetComponent<CircleCollider2D>().IsTouchingLayers())
        {
            int seconds = (int)currentTime;
            int minutes = seconds / 60;
            seconds = seconds % 60;
            int finalDeaths;
            if (practice)
            {
                finalDeaths = state.practiceDeaths;
            } else
            {
                finalDeaths = state.deaths;
            }
            string finalText = "Has completado el nivel en " + minutes + " minutos " + seconds + " segundos" +
                    "\nY has muerto un total de " + finalDeaths + " veces";
            if (!practice)
            {
                if (state.CompareRecordTime(minutes, seconds))
                {
                    text.GetComponent<Text>().text = "¡NUEVO RECORD!\n" + finalText;
                    state.endedLastGame = true;
                }
                else
                {
                    text.GetComponent<Text>().text = finalText;
                    state.endedLastGame = true;
                }
            } else
            {
                text.GetComponent<Text>().text = finalText;
                state.endedLastPractice = true;
            }

            text.SetActive(true);

            Destroy(endGame.GetComponent<CircleCollider2D>());
            endGame = null;
        }
    }
}
