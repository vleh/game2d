﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameState", menuName = "GameStates/GameState")]
public class GameState : ScriptableObject
{
    public int deaths;
    public int practiceDeaths;
    public bool endedLastGame = true;
    public bool endedLastPractice = true;
    public int totalDeaths;
    public int recordMinutes = 59;
    public int recordSeconds = 59;
    public bool practice;

    public bool CompareRecordTime(int  minutes, int seconds)
    {
        bool newRecord = false;
       if (minutes < recordMinutes)
        {
            newRecord = true;
        } else if (seconds < recordSeconds)
        {
            newRecord = true;
        }

       if (newRecord)
        {
            NewRecord(minutes, seconds); 
        }

        return newRecord;
    } 

    public void NewRecord(int minutes, int seconds)
    {
        recordMinutes = minutes;
        recordSeconds = seconds;
    }
}
