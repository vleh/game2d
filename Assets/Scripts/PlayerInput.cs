﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInput : MonoBehaviour
{
	private Rigidbody2D rb;
	public GameObject blood;
    public LayerMask groundLayers;
    public float MaxSpeed = 10;
    public float Accel = 35;
    public float JumpSpeed = 8;
    public float JumpDuration;
	public float sGravity;

    //public bool EnableDoubleJump;
    public bool wallJump = true;

    //internal checks
    bool canDoubleJump;

    float jumpDuration;

    bool jumpKeyDown = false;
    bool canVariableJump = false;

    private SpriteRenderer sprite;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
		rb = GetComponent<Rigidbody2D> ();
        this.JumpDuration = 150f;
		this.sGravity = 20f;
    }


    void Update()
    {
		bool left = Input.GetKey("left");
		bool right = Input.GetKey("right");

        if (Input.GetKeyDown("q"))
        {
            Application.Quit();
        }

        if (left) {
			if (rb.velocity.x > -this.MaxSpeed) {
				rb.AddForce (new Vector2 (-this.Accel, 0.0f));
			} else {
				rb.velocity = new Vector2 (-this.MaxSpeed, rb.velocity.y);
			}
            transform.localScale = new Vector3(-1, 1, 1);
        } else if (right) 
		{
			if (rb.velocity.x < this.MaxSpeed) {
				rb.AddForce (new Vector2 (this.Accel, 0.0f));
			} else {
				rb.velocity = new Vector2 (this.MaxSpeed, rb.velocity.y);
			}
            transform.localScale = new Vector3(1, 1, 1);
        }

		bool leftWallHit = isOnWallLeft();
		bool rightWallHit = isOnWallRight();

        bool jump = Input.GetKey("space");
        bool grounded = isGrounded ();

		if (jump) {
			if (!jumpKeyDown) {
				jumpKeyDown = true;

				if (grounded || wallJump) {
					bool wallHit = false;
					int wallHitDirection = 0;

					if (!grounded && (leftWallHit || rightWallHit)) {
						if (leftWallHit) {
							wallHit = true;
							wallHitDirection = 1;
						} else if (rightWallHit) {
							wallHit = true;
							wallHitDirection = -1;
						}
					}

					if (!wallHit) {
						if (grounded) {
							rb.velocity = new Vector2 (rb.velocity.x, this.JumpSpeed);

							jumpDuration = 0.0f;
							canVariableJump = true;
						}
					} else {
						rb.velocity = new Vector2 (this.JumpSpeed * wallHitDirection, this.JumpSpeed);

						jumpDuration = 0.0f;
						canVariableJump = true;
					}
				}
			} else if (canVariableJump) {
				jumpDuration += Time.deltaTime;

				if (jumpDuration < this.JumpDuration / 1000) {
					rb.velocity = new Vector2 (rb.velocity.x, this.JumpSpeed);
				}
			}
		} else {
			jumpKeyDown = false;
			canVariableJump = false;
		}

		bool grav = Input.GetKey("left shift");

		if (grav && !grounded)
		{
			rb.velocity += Vector2.up * Physics2D.gravity.y * sGravity * Time.deltaTime;
            rb.velocity = new Vector2(0, rb.velocity.y);
		} else if (rb.velocity.y < 0)
		{
			rb.velocity += Vector2.up * Physics2D.gravity.y * 1f * Time.deltaTime;
		}
    }

    private bool isGrounded()
    {

        bool hit = Physics2D.OverlapArea(new Vector2 (transform.position.x - 0.152f , transform.position.y - 0.159f), 
             new Vector2 (transform.position.x + 0.152f, transform.position.y - 0.161f), groundLayers);

        return hit;
    }

    private bool isOnWallLeft()
    {
        bool hit = Physics2D.OverlapArea(new Vector2(transform.position.x - 0.153f, transform.position.y - 0.159f),
             new Vector2(transform.position.x - 0.159f, transform.position.y + 0.161f), groundLayers);

		return hit;
    }

    private bool isOnWallRight()
    {
        bool hit = Physics2D.OverlapArea(new Vector2(transform.position.x - 0.153f, transform.position.y - 0.159f),
             new Vector2(transform.position.x + 0.159f, transform.position.y + 0.161f), groundLayers);

        return hit;
    }

	private void death() 
	{
		GameObject particle = Instantiate (blood, this.transform.position, Quaternion.identity);
        Destroy(particle, 1f);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision){
		if (collision.collider.CompareTag("muerte")) {
			death ();
		}
	}
}